/++ Search algorithms.
 +/
module searching;

import std.typecons : tuple, Tuple;
import optional : Optional, match;
import std.range : front, empty, popFront;
import std.array : back, popBack;
version (unittest)
{
    import unit_threaded : should, shouldApproxEqual;
    import optional : some, no;
}

/// Implement binarysearch as described in `Binary Search a Little Simpler & More Generic
/// Jules Jacobs` and `The Binary Search revisited by Netty van Gasteren and Wim Feijen`
public template binary(T)
{
    alias Result = Tuple!(T, "left", T, "right", int, "iterations");

    // dfmt off
    public Result search(T left, T right, Optional!T delegate(T, T) midpoint, bool delegate(T) predicate)
    {
        auto result = midpoint(left, right).match!(
          (T middle) => predicate(middle) ?
              search(left, middle, midpoint, predicate)
              : search(middle, right, midpoint, predicate),
          () => Result(left, right, 0)
        );
        ++result.iterations;
        return result;
    }
    // dfmt on
}

///
@("search in array") unittest
{
    import std.range : iota;
    import std.array : array;

    const haystack = 0.iota(20, 2).array;
    {
        const needle = -10;
        // dfmt off
        const result = binary!int.search(
          -1,
          cast(int)
          haystack.length,
          (l, r) => r - l > 1 ? some(l + (r - l) / 2) : no!int,
          (i) => haystack[i] >= needle,
        );
        // dfmt on
        result.left.should == -1;
        result.right.should == 0;
    }

    {
        const needle = 40;
        // dfmt off
        const result = binary!int.search(
          -1,
          cast(int) haystack.length,
          (l, r) => r - l > 1 ? some(l + (r - l) / 2) : no!int,
          (i) => haystack[i] >= needle,
        );
        // dfmt on
        result.left.should == 9;
        result.right.should == 10;
    }
    {
        const needle = 4;
        // dfmt off
        const result = binary!int.search(
          -1,
          cast(int) haystack.length,
          (l, r) => r - l > 1 ? some(l + (r - l) / 2) : no!int,
          (i) => haystack[i] >= needle,
        );
        // dfmt on
        result.left.should == 1;
        result.right.should == 2;
    }
}

///
@("search for sqrt") unittest
{
    import std.math : sqrt;

    const epsilon = 1e-3;
    // dfmt off
    auto result = binary!double.search(
      0.0,
      10.0,
      (l, r) => r - l > epsilon ? some(l + (r - l) / 2.0) : no!double,
      (f) => f * f >= 2,
    );
    // dfmt on
    result.left.shouldApproxEqual(sqrt(2.0), 0.01);
}

/++ Run a breadth first search.
 + Params:
 +     initial = called with the search queue before the search starts.
 +     visit = called with elements from the search queue and the queue itself. The first is the element ot consider as search result, the second is the searchqueue.
 +     visited = delegate that is called for all visited elements.
 +
 + Returns: Success of the search.
 +/
public auto breadthFirst(I)(void delegate(ref I[]) initial, bool delegate(ref I,
        ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        const found = visit(i, queue);
        visited(i);
        if (found)
        {
            return true;
        }
    }
    return false;
}

///
@("breadth first") unittest
{
    class Tree
    {
        int value;
        Tree left;
        Tree right;
        this(int value, Tree left, Tree right)
        {
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }

    // dfmt off
    auto tree = new Tree(1,
                          new Tree(2,
                                   new Tree(3, null, null),
                                   new Tree(4, null, null)),
                          new Tree(5,
                                   new Tree(6, null, null),
                                   new Tree(7, null, null)));
    // dfmt on
    int[] visited;
    breadthFirst((ref Tree[] queue) { queue ~= tree; }, (ref Tree node, ref Tree[] queue) {
        if (node.left !is null)
            queue ~= node.left;
        if (node.right !is null)
            queue ~= node.right;
        return false; // this test will not find anything
    }, (ref Tree node) { visited ~= node.value; }).should == false;
    visited.should == [1, 2, 5, 3, 4, 6, 7];

    visited = [];
    // dfmt off
    breadthFirst(
      (ref Tree[] queue)
      {
          queue ~= tree;
      },
      (ref Tree node, ref Tree[] queue)
      {
          if (node.value == 4) return true;
          if (node.left !is null) queue ~= node.left;
          if (node.right !is null) queue ~= node.right;
          return false;
      },
      (ref Tree node)
      {
          visited ~= node.value;
      }).should == true;
    // dfmt on
    visited.should == [1, 2, 5, 3, 4];
}

/++ Run a depth first search.
 + Params:
 +     initial = called with the search queue before the search starts.
 +     visit = called with elements from the search queue and the queue itself. The first is the element ot consider as search result, the second is the searchqueue.
 +     visited = delegate that is called for all visited elements.
 +
 + Returns: Success of the search.
 +/
bool depthFirst(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        const found = visit(i, queue);
        visited(i);
        if (found)
        {
            return true;
        }
    }
    return false;
}

///
@("depth first") unittest
{
    class Tree
    {
        int value;
        Tree left;
        Tree right;
        this(int value, Tree left, Tree right)
        {
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }

    // dfmt off
    auto tree = new Tree(1,
                          new Tree(2,
                                   new Tree(3, null, null),
                                   new Tree(4, null, null)),
                          new Tree(5,
                                   new Tree(6, null, null),
                                   new Tree(7, null, null)));

    int[] visited;
    depthFirst!Tree(
      (ref Tree[] queue)
      {
          queue ~= tree;
      },
      (ref Tree element, ref Tree[] queue)
      {
          if (element.right !is null) queue ~= element.right;
          if (element.left !is null) queue ~= element.left;
          return false; // walk the whole tree
      },
      (ref Tree element)
      {
          visited ~= element.value;
      },
    ).should == false;
    visited.should == [1, 2, 3, 4, 5, 6, 7];

    visited = [];
    depthFirst!Tree(
      (ref Tree[] queue)
      {
          queue ~= tree;
      },
      (ref Tree element, ref Tree[] queue)
      {
          if (element.value == 4) return true;
          if (element.right !is null) queue ~= element.right;
          if (element.left !is null) queue ~= element.left;
          return false;
      },
      (ref Tree element)
      {
          visited ~= element.value;
      },
    ).should == true;
    visited.should == [1, 2, 3, 4];
}

