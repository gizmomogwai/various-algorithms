/++
 + Iteration helpers.
 +/
module iteration;

import std.range : empty, front, popFront;

version (unittest)
{
    import unit_threaded : should;
    import std.array : array;
}

/// like std.range : chunk, but returns all subranges of requested length
auto eachChunkOf(R)(R range, size_t length)
{
    struct EachChunkOf
    {
        bool empty()
        {
            return range.length < length;
        }

        auto front()
        {
            return range[0 .. length];
        }

        void popFront()
        {
            range.popFront;
        }
    }

    return EachChunkOf();
}

///
@("eachChunkOf") unittest
{
    [1, 2, 3].eachChunkOf(2).array.should == [[1, 2], [2, 3]];

    [1, 2, 3].eachChunkOf(4).empty.should == true;

    [1, 2, 3, 4].eachChunkOf(4).array.should == [[1, 2, 3, 4]];
}
