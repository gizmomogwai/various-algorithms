/++
 + License: MIT
 + Copyright: Copyright (c) 2023, Christian Koestlin
 + Authors: Christian Koestlin
 +/
import unit_threaded : runTestsMain;

mixin runTestsMain!("iteration", "searching",);
